#!/usr/bin/python
"""A plugin to execute cucumber scenarios and format the output in a nagios way"""

#---- Import modules ----
import os
import sys
import json
import getopt

#---- Default variables ----
project=''
debug=False
command="cucumber"
command_options="HEADLESS=true --format json"

counters={
    'failed' : 0,
    'passed' : 0,
    'duration' : 0,
    'features' : 0,
    'steps' : 0,
}


nagios_exit_code = { 
    'OK' : 0,
    'WARNING' : 1,
    'CRITICAL' : 2,
    'UNKNOWN' : 3
}

#---- Functions ----
def usage():
    print """
    Usage:
        check_cucumber.py -p|--project CUCUMBER_PROJECT_PATH

    Options:

        -p|--project PROJECT_PATH : the path of your cucumber project
        -f|--feature FEATURE_NAME ! the feature located in the features dir of your project
        -d|--debug : Print debug output
        -h|--help : display this help
    """

def get_scenario_output():
    """ Get the scenario output """
    os.chdir(project)
    cwd = os.getcwd()

    if debug:
        print command + ' ' + command_options

    return os.popen(command + ' ' + command_options)

def parse_output(json_output):
    """Parse the output and return : number of scenarios, steps, details and execution time"""

    #For each feature, count the total duration, total number of steps, total features...
    for feature in json_output:
        counters['features'] += 1
        for element in feature['elements']:
            for step in element['steps']:
                counters['steps'] += 1
                counters['duration'] += step['result']['duration']

                if step['result']['status'] == "passed":
                    counters['passed'] += 1
                else:
                    counters['failed'] += 1


def get_alert_criticity():
    """Give the right alert criticity"""
    global worst_state

    if counters['failed'] > 0:
        worst_state = 'CRITICAL'
    else:
        worst_state = 'OK'

def print_nagios_output():
    """Make the output in a Nagios compatible format"""
    output_expected_example="CUCUMBER OK - Critical: 0, Warning: 0, 3 okay | passed=3; failed=0; nosteps=0; total=3; time=5"

    format_variables = (worst_state, 
                        counters['passed'], 
                        counters['failed'], 
                        project_name, 
                        counters['passed'], 
                        counters['failed'], 
                        counters['steps'], 
                        counters['features'], 
                        counters['duration']
                        )

    nagios_output = "CUCUMBER %s - Passed: %s, Failed: %s for project %s | passed=%s; failed=%s; steps=%s, features=%s, duration=%s" % format_variables
    print nagios_output

#---- Main ----
def main():
    #global vars
    global project, project_name, debug, command

    #options
    short_options = "hdp:f:"
    long_options = ["help", "debug", "project=", "feature="]

    try:
        opts, args = getopt.getopt(sys.argv[1:], short_options, long_options)
    except getopt.GetoptError as err:
        print(err)
        usage()
        sys.exit(2)

    for opt, arg in opts:
        if opt in ('-h', '--help'):
            usage()
            sys.exit()
        elif opt in ('-d', '--debug'):
            debug = True
        elif opt in ('-p', '--project'):
            project = arg
        elif opt in ('-f', '--feature'):
            feature = arg
            command=command + " features/" + feature
        else:
            print("Unknonw option {}".format(opt))
            sys.exit(2)

    if not project:
        print "option -p|--project required"
        sys.exit(2)

    #Computed variables
    project_name=os.path.basename(os.path.abspath(project))

    #Core
    scenario_json_output = json.loads(get_scenario_output().read())
    parse_output(scenario_json_output)
    get_alert_criticity()

    if debug:
        print scenario_json_output
        print counters

    print_nagios_output()

    sys.exit(nagios_exit_code[worst_state])

main()
