Cucumber-nagios-plugin
===========================

## Author

Name: Paul Premont
Email: paul@premont.fr

## Purpose

This plugin allow the administrators to schedule a cucumber scenario from a Nagios like supervisor system.

It is the same goal than the project [cucumber-nagios](https://github.com/auxesis/cucumber-nagios) but without write a Formatter.

It use the output of the json Formatter (more simple to maintain).

## Usage

    /usr/lib/nagios/plugins/check_cucumber.py -p /path/to/cucumber/scenario

### Environment

To use it with nagios, you should set a bash environment for the nagios user and maybe use a bash_wrapper script to execute it.  
By example to load a Firefox Profile.

### Output

Basic output example :

   CUCUMBER OK - Passed: 4, Failed: 0 for project cucumber-score | passed=4; failed=0; steps=4, features=1, duration=38784020223

Th duration is in nanoseconds
